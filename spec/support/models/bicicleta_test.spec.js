var Bicicleta = require('../../../models/bicicleta');

beforeEach(() => {
    Bicicleta.allBicis = [];
})

describe('bicicleta.allBicis', () =>{
    it('comienza vacio', ()=>{
        console.log("init bicis", Bicicleta.allBicis.length)
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('bicicleta.add',() => {
    it("agregamos una", () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);

        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);

        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('bicicleta.findById', () => {
    it('buscar por id', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var biciUno = new Bicicleta(1, "verde", "urbana");
        var biciDos = new Bicicleta(2, "roja", "cabeza");

        Bicicleta.add(biciUno);
        Bicicleta.add(biciDos);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(biciUno.color);
        expect(targetBici.modelo).toBe(biciUno.modelo)

    });
});

describe('removeById', () => {
    it('eliminar por id', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var biciUno = new Bicicleta(1, "verde", "urbana");
        var biciDos = new Bicicleta(2, "roja", "cabeza");

        Bicicleta.add(biciUno);
        Bicicleta.add(biciDos);

        for(var i = 0; i < Bicicleta.allBicis.length; i++){
            
            if(Bicicleta.allBicis[i].id == biciUno.id){
                expect(Bicicleta.allBicis[i].id).toBe(1)
                Bicicleta.allBicis.splice(i, 1);
                break;
            }
        }

        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0].id).toBe(2)

    })
})