var Bicicleta = require('../../../models/bicicleta');
var request = require('request');
var server = require('../../../bin/www');

beforeEach(()=>{
    Bicicleta.allBicis = [];
})



describe('bicicleta API', () => {
    describe('GET BICICLETA /', () => {
        it('status 200', () => {
          expect(Bicicleta.allBicis.length).toBe(0);            

          var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
          Bicicleta.add(a);

          request.get('http://localhost:5000', (error,response,body) => {
              expect(response.statusCode).toBe(200);
          });
        });
    });
    describe('POST BICICLETA', () => {
        it('status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 10, "color": "rojo", "modelo": "montaña", "lat": -34, "lon": -23}';           
            request.post({
                headers: headers, 
                url: 'http://localhost:5000/api/bicicletas/create',
                body: aBici 
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('rojo');
                done()
            });
        })
    })

    describe('DELETE BICICLETA', () => {
        it('status 204', () => {
            var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
            Bicicleta.add(a);

            request.delete( `http://localhost:5000/api/bicicletas/delete?id=${a.id}`, (error, response, body) => {                
                expect(response.statusCode).toBe(204)
            })
        })
    })
    describe('EDIT BICICLETA', () => {
        it('status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
            Bicicleta.add(a);
            var aBici = '{"id": 1, "color": "rojo", "modelo": "montaña", "lat": -34, "lon": -23}'; 

            request.post({
                headers: headers,
                url: `http://localhost:5000/api/bicicletas/edit?id=${a.id}`,
                body: aBici
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).modelo).toBe('montaña')
                done()
            })            
        })
    })
})


